// C++17 standard
// created by cicek on May 20, 2021 5:50 PM

#include <iostream>

void print() // this print lives in the global namespace
{
    std::cout << " there\n";
}

namespace foo // define a namespace named foo
{
// This doSomething() belongs to namespace foo
int doSomething(int x, int y) { return x + y; }
void print() // this print lives in the foo namespace
{
    std::cout << "Hello";
}
void printHelloThere()
{
    print(); // calls print() in foo namespace
    ::print(); // calls print() in global namespace
}
}

namespace goo // define a namespace named goo
{
// This doSomething() belongs to namespace goo
int doSomething(int x, int y) { return x - y; }
}

int main()
{
    // use the doSomething() that exists in namespace foo
    std::cout << goo::doSomething(4, 3) << '\n';
    std::cout << foo::doSomething(4, 3) << '\n';

//    foo::print(); // call print() in foo namespace
//    ::print(); // call print() in global namespace (same as just calling print() in this case)

    foo::printHelloThere();

    return 0;
}
