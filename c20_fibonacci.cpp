// C++17 standard
// created by cicek on Jun 27, 2021 1:05 PM

#include <iostream>

int fib(int a)
{
    if (a == 0)
        return 0;
    if (a == 1)
        return 1;

    return fib(a - 1) + fib(a - 2);
}

int main()
{
    for (int i = 0; i <= 6; ++i)
        std::cout << fib(i) << '\n';

    return 0;
}
