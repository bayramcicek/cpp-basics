// C++17 standard
// created by cicek on May 06, 2021 7:04 PM

#include <iostream>
#define PRINT_BOB

// Conditional compilation: #ifdef, #ifndef, and #endif.

/*
 * The #ifdef preprocessor directive allows the preprocessor to check whether an
 * identifier has been previously #defined. If so, the code between the #ifdef and
 * matching #endif is compiled. If not, the code is ignored.
 */

int main()
{
#ifdef PRINT_JOE
    std::cout << "Joe\n"; // if PRINT_JOE is defined, compile this code
#endif

#ifdef PRINT_BOB // Because PRINT_BOB has been #defined
    std::cout << "Bob\n"; // if PRINT_BOB is defined, compile this code
#endif
    // #ifndef is the opposite of #ifdef, in that it allows you to check
    // whether an identifier has NOT been #defined yet.
#ifndef YELLOW_1
    // This program prints “Yellow not defined”, because YELLOW_1 was never #defined.
    std::cout << "Yellow not defined";
#endif

    std::cout << "Joe\n";
#if 0 // Don't compile anything starting here
    std::cout << "Bob\n";
    std::cout << "Steve\n";
#endif // until this point
    // This provides a convenient way to “comment out” code that contains multi-line comments.

    /*----------------------*/

#define FOO 9 // Here's a macro substitution
#ifdef FOO // This FOO does not get replaced because it’s part of another preprocessor directive
    std::cout
        << FOO; // This FOO gets replaced with 9 because it's part of the normal code
#endif

    /*
     * Even though PRINT was defined in main.cpp, that doesn’t have any impact
     * on any of the code in function.cpp (PRINT is only #defined from the point
     * of definition to the end of main.cpp). This will be of consequence when
     * we discuss header guards in a future lesson.
     */

    return 0;
}
