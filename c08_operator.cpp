// C++17 standard
// created by cicek on May 16, 2021 8:53 PM

#include <cmath>
#include <iostream>

int main()
{
    double x{ std::pow(3.0, 4.0) }; // 3 to the 4th power
    std::cout << x << '\n';

    // conditional operator (?:)
    int larger = (52 > 6) ? 10 : 5;
    std::cout << larger << '\n';

    return 0;
}
