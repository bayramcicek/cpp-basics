// C++17 standard
// created by cicek on Jun 26, 2021 7:19 PM

#include <iostream>

int foo(int x) { return x; }

int main()
{
    int (*fcnPtr)(int){ &foo }; // Initialize fcnPtr with function foo

    //  via explicit dereference
    (*fcnPtr)(5); // call function foo(5) through fcnPtr.

    // via implicit dereference
    fcnPtr(5); // call function foo(5) through fcnPtr.

    return 0;
}
