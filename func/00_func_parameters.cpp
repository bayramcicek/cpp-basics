// C++17 standard
// created by cicek on Jun 26, 2021 7:04 AM

#include <iostream>
#include <tuple>

std::tuple<int, double>
returnTuple() // return a tuple that contains an int and a double
{
    return { 5, 6.7 };
}

int main()
{
    std::tuple s{ returnTuple() }; // get our tuple
    std::cout << std::get<0>(s) << ' ' << std::get<1>(s)
              << '\n'; // use std::get<n> to get the nth element of the tuple

    return 0;
}