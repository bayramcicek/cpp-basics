// C++17 standard
// created by cicek on May 15, 2021 9:35 PM

#include "constants.h"
#include <iostream>

int main()
{
    int radius{ 4 };

    double circumference{ 2.0 * radius * constants::pi };
    std::cout << "The circumference is: " << circumference << '\n';

    return 0;
}
