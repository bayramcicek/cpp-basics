// C++17 standard
// created by cicek on May 15, 2021 9:31 PM

#ifndef CPP_PROJECTS_CONSTANTS_H
#define CPP_PROJECTS_CONSTANTS_H

/*
 * There are multiple ways to facilitate this within C++, but the following is probably easiest:

1) Create a header file to hold these constants
2) Inside this header file, declare a namespace (we’ll talk more about this in lesson 6.2 -- User-defined namespaces)
3) Add all your constants inside the namespace (make sure they’re constexpr in C++11/14, or inline constexpr in C++17 or newer)
4) #include the header file wherever you need it
 */

namespace constants
{
inline constexpr double pi{
    3.14159
}; // inline constexpr is C++17 or newer only
inline constexpr double avogadro{ 6.0221413e23 };
inline constexpr double my_gravity{
    9.2
}; // m/s^2 -- gravity is light on this planet
// ... other related constants
}

#endif //CPP_PROJECTS_CONSTANTS_H
