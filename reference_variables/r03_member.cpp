// C++17 standard
// created by cicek on Jun 24, 2021 4:10 PM

#include <iostream>

struct Paw
{
    int claws{};
};

struct Animal
{
    [[maybe_unused]] std::string name{};
    Paw paw{};
};

int main()
{
    Animal puma{ "Puma", { 5 } };

    Animal* pointy{ &puma };

    // pointy is a pointer, use ->
    // paw is not a pointer, use .
    std::cout << pointy->paw.claws << '\n';

    return 0;
}
