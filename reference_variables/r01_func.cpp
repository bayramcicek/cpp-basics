// C++17 standard
// created by cicek on Jun 24, 2021 1:59 PM

#include <iostream>

// // ref is a reference to the argument passed in, not a copy
void Change(int &var) { var = 49; }

int main()
{
    int a{ 5 };
    std::cout << a << '\n'; // 5

    //    note that this argument does not need to be a reference
    Change(a);

    std::cout << a << '\n'; // 49

    return 0;
}
