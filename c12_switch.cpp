// C++17 standard
// created by cicek on May 29, 2021 11:16 AM

#include <iostream>

int main()
{
    int test{ 23 };

    switch (test)
    {
        case 1:
            std::cout << "-> 1";
            break;
        case 23:
            std::cout << "-> 23";
            break;
        default:
            std::cout << "Unknown";
            break;
    }

    return 0;
}
