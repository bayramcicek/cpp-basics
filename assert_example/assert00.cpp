// C++17 standard
// created by cicek on May 29, 2021 2:23 PM

#include <iostream>
/*
 * An assertion is an expression that will be true unless there is a bug in the
 * program. If the expression evaluates to true, the assertion statement does
 * nothing. If the conditional expression evaluates to false, an error message
 * is displayed and the program is terminated (via std::abort). This error message
 * typically contains the expression that failed as text, along with the name of
 * the code file and the line number of the assertion. This makes it very easy to
 * tell not only what the problem was, but where in the code the problem occurred.
 * This can help with debugging efforts immensely.

 */

#include <cassert> // for assert()
#include <cmath> // for std::sqrt
// Although we told you previously to avoid preprocessor macros, asserts are one of the few
// preprocessor macros that are considered acceptable to use. We encourage you to use assert statements liberally throughout your code.

// assert(found && "Car could not be found in database");
// Use assertions to document cases that should be logically impossible

double calculateTimeUntilObjectHitsGround(double initialHeight, double gravity)
{
    assert(
        gravity
        > 0.0); // The object won't reach the ground unless there is positive gravity.
    /*
     * /assert_example/assert00.cpp:22: double
     * calculateTimeUntilObjectHitsGround(double, double): Assertion `gravity > 0.0' failed.
     */

    if (initialHeight <= 0.0)
    {
        // The object is already on the ground. Or buried.
        return 0.0;
    }

    return std::sqrt((2.0 * initialHeight) / gravity);
}

int main()
{
    std::cout << "Took " << calculateTimeUntilObjectHitsGround(100.0, -9.8)
              << " second(s)\n";

    return 0;
}
