// C++17 standard
// created by cicek on May 29, 2021 2:27 PM

#include <iostream>

static_assert(sizeof(long) == 8, "long must be 8 bytes");
static_assert(sizeof(int) == 4, "int must be 4 bytes");
//  static_assert is evaluated by the compiler, the condition must be able to be evaluated at compile time.
int main()
{
    std::cout << "Hello world\n";
    return 0;
}
