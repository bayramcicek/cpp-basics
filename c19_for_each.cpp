// C++17 standard
// created by cicek on Jun 24, 2021 4:25 PM

#include <iostream>

int main()
{
    //    constexpr int fibonacci[]{ 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89 };
    //    for (auto number : fibonacci) // type is auto, so number has its type deduced from the fibonacci array
    //    { // number will be a copy of the current array element
    //        std::cout << number << ' ';
    //    }
    /*
    std::string array[]{ "peter", "likes", "frozen", "yogurt" };
    for (auto& element: array) // The ampersand makes element a reference to the actual array element, preventing a copy from being made
    {
        std::cout << element << ' ';
    }
    */

    std::string array[]{ "peter", "likes", "frozen", "yogurt" };
    for (
        const auto &element :
        array) // element is a const reference to the currently iterated array element
    {
        std::cout << element << ' ';
    }



    return 0;
}
