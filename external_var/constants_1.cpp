// C++17 standard
// created by cicek on May 24, 2021 1:57 AM

namespace constants_1
{
/*Because global symbolic constants should be namespaced (to avoid naming
 * conflicts with other identifiers in the global namespace), the use of a “g_” naming prefix is not necessary.*/
// actual global variables
extern const double pi{ 3.14159 };
extern const double avogadro{ 6.0221413e23 };
extern const double my_gravity{
    9.2
}; // m/s^2 -- gravity is light on this planet
}
