// C++17 standard
// created by cicek on May 24, 2021 1:58 AM

#ifndef CPP_PROJECTS_CONSTANTS_1_H
#define CPP_PROJECTS_CONSTANTS_1_H

namespace constants_1
{
// since the actual variables are inside a namespace, the forward declarations need to be inside a namespace as well
extern const double pi;
extern const double avogadro;
extern const double my_gravity;
}

#endif //CPP_PROJECTS_CONSTANTS_1_H
