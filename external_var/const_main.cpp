// C++17 standard
// created by cicek on May 24, 2021 2:10 AM

#include "constants_1.h" // include all the forward declarations
#include <iostream>

int main()
{
    //    std::cout << "Enter a radius: ";
    int radius{ 5 };
    //    std::cin >> radius;

    std::cout << "The circumference is: " << 2.0 * radius * constants_1::pi
              << '\n';

    return 0;
}

/*
 * Now the symbolic constants will get instantiated
 * only once (in constants.cpp) instead of in each
 * code file where constants.h is #included, and all
 * uses of these constants will be linked to the version
 * instantiated in constants.cpp. Any changes made to constants.cpp will
 * require recompiling only constants.cpp.
 */

/*
 * However, there are a couple of downsides to this method. First, these constants
 * are now considered compile-time constants only within the file they are actually
 * defined in (constants.cpp). In other files, the compiler will only see the forward
 * declaration, which doesn’t define a constant value (and must be resolved by the linker).
 *
 * This means in other files, these are treated as runtime constant values, not compile-time
 * constants. Thus outside of constants.cpp, these variables can’t be used anywhere that requires
 * a compile-time constant. Second, because compile-time constants can typically be optimized more
 * than runtime constants, the compiler may not be able to optimize these as much.
 */
