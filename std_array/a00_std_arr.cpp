// C++17 standard
// created by cicek on Jun 24, 2021 5:48 PM

#include <iostream>
#include <array>

int main()
{
    std::array<int, 3> myarray{}; // declare an integer array with length 3
    std::array<int, 5> myArray = { 9, 7, 5, 3, 1 }; // initializer list
    std::array<int, 5> myArray2 { 9, 7, 5, 3, 1 }; // list initialization

//    std::array<int, > myArray { 9, 7, 5, 3, 1 }; // illegal, array length must be provided
//    std::array<int> myArray { 9, 7, 5, 3, 1 }; // illegal, array length must be provided
    std::array myArray12 { 9, 7, 5, 3, 1 }; // The type is deduced to std::array<int, 5>
    std::array myArray23 { 9.7, 7.31 }; // The type is deduced to std::array<double, 2>

    myArray = { 0, 1, 2, 3, 4 }; // okay
    myArray = { 9, 8, 7 }; // okay, elements 3 and 4 are set to zero!
//    myArray = { 0, 1, 2, 3, 4, 5 }; // not allowed, too many elements in initializer list!

    std::cout << "length: " << myArray.size() << '\n'; // 5

    return 0;
}
