// C++17 standard
// created by cicek on Jun 24, 2021 6:00 PM

#include <algorithm> // for std::sort
#include <array>
#include <iostream>

// we passed std::array by (const) reference. This is to prevent the compiler
// from making a copy of the std::array when the std::array was passed to the function (for performance reasons).
void printLength(const std::array<double, 5> &myArray)
{ // Always pass std::array by reference or const reference
    std::cout << "length: " << myArray.size() << '\n';
}

int main()
{
    std::array myArray{ 9.0, 7.2, 5.4, 3.6, 1.8 };
    printLength(myArray);

    for (auto element : myArray)
        std::cout << element << '\n';

    std::sort(myArray.begin(), myArray.end());

    for (auto element : myArray)
        std::cout << element << '\n';

    return 0;
}
