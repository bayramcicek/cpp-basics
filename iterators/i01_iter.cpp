// C++17 standard
// created by cicek on Jun 24, 2021 9:22 PM

#include <array>
#include <iostream>
#include <iterator> // For std::begin and std::end

int main()
{
    std::array array{ 1, 2, 3 };

    // Ask our array for the begin and end points (via the begin and end member functions).
    auto begin{ array.begin() };
    auto end{ array.end() };

    // or
//    // Use std::begin and std::end to get the begin and end points.
//    auto begin{ std::begin(array) };
//    auto end{ std::end(array) };

    for (auto p{ begin }; p != end; ++p) // ++ to move to next element.
    {
        std::cout << *p << ' '; // Indirection to get value of current element.
    }
    std::cout << '\n';

    return 0;
}
