// C++17 standard
// created by cicek on Jun 24, 2021 7:37 PM

#include <iostream>
#include <vector>

int main()
{
    std::vector array{ 0, 1, 2 };
    array.resize(5); // set size to 5

    std::cout << "The length is: " << array.size() << '\n';

    for (int i : array)
        std::cout << i << ' ';

    std::cout << '\n';

    //The length is: 5
    //0 1 2 0 0

    return 0;
}
