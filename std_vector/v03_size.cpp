// C++17 standard
// created by cicek on Jun 24, 2021 7:39 PM

#include <iostream>
#include <vector>

int main()
{
    // Using direct initialization, we can create a vector with 5 elements,
    // each element is a 0. If we use brace initialization, the vector would
    // have 1 element, a 5.
    std::vector<int> array(5);

    std::cout << "The length is: " << array.size() << '\n';

    for (int i : array)
        std::cout << i << ' ';

    //The length is: 5
    //0 0 0 0 0
    std::cout << '\n';

    std::vector<bool> array1{ true, false, false, true, true };
    std::cout << "The length is: " << array1.size() << '\n';

    for (int i : array1)
        std::cout << i << ' ';

    //The length is: 5
    //1 0 0 1 1

    return 0;
}
