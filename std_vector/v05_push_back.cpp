// C++17 standard
// created by cicek on Jun 27, 2021 10:58 AM

#include <iostream>
#include <vector>

/*
 * Although std::vector can be used as a dynamic array, it can also be used as a stack.
 * To do this, we can use 3 functions that match our key stack operations:
 */

//push_back() pushes an element on the stack.
//back() returns the value of the top element on the stack.
//pop_back() pops an element off the stack.

void printStack(const std::vector<int> &stack)
{
    for (auto element : stack)
        std::cout << element << ' ';
    std::cout << "(cap " << stack.capacity() << " length " << stack.size()
              << ")\n";
}

int main()
{
    std::vector<int> stack{};

    printStack(stack);

    stack.push_back(5); // push_back() pushes an element on the stack
    printStack(stack);

    stack.push_back(3);
    printStack(stack);

    stack.push_back(2);
    printStack(stack);

    std::cout << "top: " << stack.back()
              << '\n'; // back() returns the last element

    stack.pop_back(); // pop_back() pops an element off the stack
    printStack(stack);

    stack.pop_back();
    printStack(stack);

    stack.pop_back();
    printStack(stack);

    return 0;
}