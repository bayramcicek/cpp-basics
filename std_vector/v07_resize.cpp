// C++17 standard
// created by cicek on Jun 27, 2021 11:08 AM

#include <iostream>
#include <vector>

int main()
{
    std::vector<int> v{ 0, 1, 2, 3, 4 };
    std::cout << "size: " << v.size() << "  cap: " << v.capacity() << '\n';

    v.push_back(5); // add another element
    std::cout << "size: " << v.size() << "  cap: " << v.capacity() << '\n';

    //size: 5  cap: 5
    //size: 6  cap: 10

    return 0;
}
