// C++17 standard
// created by cicek on May 06, 2021 1:41 AM

/*
 * The #define directive can be used to create a macro. In C++,
 * a macro is a rule that defines how input text is converted into replacement output text.
There are two basic types of macros: object-like macros, and function-like macros.
 */

// Object-like macros with substitution text
#define MY_NAME "bayram"
// The preprocessor converts the above into the following: bayram
// We recommend avoiding these kinds of macros altogether, as there are better ways to do this
// kind of thing. We discuss this more in lesson 4.14 -- Const, constexpr, and symbolic constants.

/*
 *Preprocessor directives (often just called directives) are instructions
 * that start with a # symbol and end with a newline (NOT a semicolon).
 */

#include <iostream>
/*
 * When the preprocessor runs on this program,
 * the preprocessor will replace #include <iostream> with the preprocessed
 * contents of the file named “iostream”.
 */
int main()
{
    std::cout << "Hello world\n" << MY_NAME;
    return 0;
}
