// C++17 standard
// created by cicek on Jun 23, 2021 10:30 AM

#include <iostream>

/*A null value is a special value that means the pointer is not pointing at anything.
 * A pointer holding a null value is called a null pointer.
 * */

int main()
{
    float *ptr{ nullptr }; // ptr is now a null pointer

    float *ptr2; // ptr2 is uninitialized
    ptr2 = nullptr; // ptr2 is now a null pointer

    //C++ will implicitly convert nullptr to any pointer type. So in the above example,
    // nullptr is implicitly converted to an integer pointer, and then the value of nullptr assigned to ptr.
    // This has the effect of making integer pointer ptr a null pointer.

    // pointers convert to boolean false if they are null, and boolean true if they are non-null
    // as of C++11, NULL can be defined as nullptr instead
    if (ptr) // false
        std::cout << "ptr is pointing to a double value.";
    else
        std::cout << "ptr is a null pointer.";

    return 0;
}
