// C++17 standard
// created by cicek on Jun 23, 2021 11:19 AM

#include <iostream>

int main()
{
    // When calculating the result of a pointer arithmetic expression,
    // the compiler always multiplies the integer operand by the size of the object being pointed to. This is called scaling.

    int array[]{ 9, 7, 5, 3, 1 };

    std::cout << &array[1] << '\n'; // print memory address of array element 1
    std::cout << array+1 << '\n'; // print memory address of array pointer + 1

    std::cout << array[1] << '\n'; // prints 7
    std::cout << *(array+1) << '\n'; // prints 7 (note the parenthesis required here)

    /*It turns out that when the compiler sees the subscript operator ([]), it actually translates that into a pointer addition and indirection!
     * Generalizing, array[n] is the same as *(array + n), where n is an integer.
     * The subscript operator [] is there both to look nice and for ease of use (so you don’t have to remember the parenthesis).*/

    return 0;
}
