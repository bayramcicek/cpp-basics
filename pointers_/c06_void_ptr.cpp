// C++17 standard
// created by cicek on Jun 24, 2021 4:56 PM

#include <iostream>

enum class Type
{
    INT,
    FLOAT,
    CSTRING
};

void printValue(void *ptr, Type type)
{
    switch (type)
    {
        case Type::INT:
            std::cout << *static_cast<int *>(ptr)
                      << '\n'; // cast to int pointer and perform indirection
            break;
        case Type::FLOAT:
            std::cout << *static_cast<float *>(ptr)
                      << '\n'; // cast to float pointer and perform indirection
            break;
        case Type::CSTRING:
            std::cout << static_cast<char *>(ptr)
                      << '\n'; // cast to char pointer (no indirection)
            // std::cout knows to treat char* as a C-style string
            // if we were to perform indirection through the result, then we'd just print the single char that ptr is pointing to
            break;
    }
}

int main()
{
    //    int nValue;
    //    float fValue;
    //
    //    struct Something
    //    {
    //        int n;
    //        float f;
    //    };
    //
    //    Something sValue{};
    //
    //    void *ptr;
    //    ptr = &nValue; // valid
    //    ptr = &fValue; // valid
    //    ptr = &sValue; // valid

    //    int value{ 5 };
    //    void *voidPtr{ &value };
    //
    //// std::cout << *voidPtr << '\n'; // illegal: Indirection through a void pointer
    //    int *intPtr{ static_cast<int*>(voidPtr) }; // however, if we cast our void pointer to an int pointer...
    //    std::cout << *intPtr << '\n'; // then we can use indirection through it like normal

    int nValue{ 5 };
    float fValue{ 7.5f };
    char szValue[]{ "Mollie" };

    printValue(&nValue, Type::INT);
    printValue(&fValue, Type::FLOAT);
    printValue(szValue, Type::CSTRING);

    return 0;
}
