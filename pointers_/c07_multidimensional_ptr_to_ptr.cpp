// C++17 standard
// created by cicek on Jun 24, 2021 5:09 PM

#include <iostream>

int main()
{
//    int **ptrptr; // pointer to a pointer to an int, two asterisks
    int value = 5;

    int *ptr = &value;
    std::cout << *ptr; // Indirection through pointer to int to get int value

    int **ptrptr = &ptr;
    std::cout << **ptrptr; // first indirection to get pointer to int, second indirection to get int value

    return 0;
}
