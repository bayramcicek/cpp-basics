// C++17 standard
// created by cicek on Jun 23, 2021 9:47 AM

#include <iostream>

struct Something
{
    int x{};
    int y{};
    int z{};
};

int main()
{
    char *chPtr{}; // chars are 1 byte
    int *iPtr{}; // ints are usually 4 bytes

    Something *somethingPtr{}; // *somethingPtr is probably 12 bytes

    std::cout << sizeof(chPtr) << '\n'; // prints 8
    std::cout << sizeof(iPtr) << '\n'; // prints 8
    std::cout << sizeof(somethingPtr) << '\n'; // prints 8
    std::cout << sizeof(*somethingPtr) << '\n'; // prints 12
    return 0;
}
