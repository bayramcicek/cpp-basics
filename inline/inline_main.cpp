// C++17 standard
// created by cicek on May 24, 2021 2:43 AM

#include "inline.h"
#include <iostream>

int main()
{
//    std::cout << "Enter a radius: ";
    int radius{10};
//    std::cin >> radius;

    std::cout << "The circumference is: " << 2.0 * radius * inline_consts::pi
              << '\n';

    return 0;
}

/*
 * We can include constants.h into as many code files as we want, but these
 * variables will only be instantiated once and shared across all code files.
 */
