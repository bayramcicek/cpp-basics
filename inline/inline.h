// C++17 standard
// created by cicek on May 24, 2021 2:43 AM

#ifndef CPP_PROJECTS_INLINE_H
#define CPP_PROJECTS_INLINE_H

// define your own namespace to hold constants
namespace inline_consts
{
inline constexpr double pi{ 3.14159 }; // note: now inline constexpr
inline constexpr double avogadro{ 6.0221413e23 };
inline constexpr double my_gravity{
    9.2
}; // m/s^2 -- gravity is light on this planet
// ... other related constants
}

#endif //CPP_PROJECTS_INLINE_H
