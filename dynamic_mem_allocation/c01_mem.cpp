// C++17 standard
// created by cicek on Jun 24, 2021 9:12 AM

#include <iostream>

int main()
{
    /*A pointer that is pointing to deallocated memory is called a dangling pointer.
     * Indirection through- or deleting a dangling pointer will lead to undefined behavior.
     * Consider the following program:*/

    int* ptr1{ new int }; // dynamically allocate an integer
    *ptr1 = 7; // put a value in that memory location

    delete ptr1; // return the memory to the operating system. ptr is now a dangling pointer.

    // 1683759947
    std::cout << *ptr1; // Indirection through a dangling pointer will cause undefined behavior
    delete ptr1; // trying to deallocate the memory again will also lead to undefined behavior.
    // free(): double free detected in tcache 2
    //
    //Process finished with exit code 134

    return 0;
}
