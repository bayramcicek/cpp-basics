// C++17 standard
// created by cicek on Jun 24, 2021 10:49 AM

#include <iostream>

void EnterEachName(std::string *Arr, std::size_t length)
{
    for (size_t i = 0; i < length; ++i)
    {
        std::string aName{};
        std::cout << "Enter name #" << (i + 1) << ": ";
        std::cin >> aName;

        *(Arr + i) = aName;
    }
}

void SortMe(std::string *Arr, std::size_t length)
{
    for (size_t i = length - 1; i > 0; --i)
    {
        for (size_t j = 0; j <= (i - 1); ++j)
        {
            if ((*(Arr + j)) > (*(Arr + j + 1)))
                std::swap((*(Arr + j)), (*(Arr + j + 1)));
        }
    }
}

int main()
{
    std::cout << "How many names would you like to enter?: ";
    std::size_t l_names{};
    std::cin >> l_names;

    auto *nameString = new std::string[l_names];
    EnterEachName(nameString, l_names);

    //    for (size_t a = 0; a < l_names; ++a)
    //    {
    //        std::cout << *(nameString + a) << '\n';
    //    }

    SortMe(nameString, l_names);

    /*  // Sort the array
  std::sort(names, names + length);*/

    std::cout << "\nsorted list: \n";

    for (size_t a = 0; a < l_names; ++a)
    {
        std::cout << *(nameString + a) << '\n';
    }

    // https://www.learncpp.com/cpp-tutorial/dynamically-allocating-arrays/

    delete[] nameString;

    return 0;
}
