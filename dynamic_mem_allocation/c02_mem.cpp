// C++17 standard
// created by cicek on Jun 24, 2021 9:15 AM

#include <iostream>

int main()
{
    // If ptr is non-null, the dynamically allocated variable will be deleted. If it is null, nothing will happen.

//    int value = 5;
//    int* ptr{ new int{} }; // allocate memory
//    ptr = &value; // old address lost, memory leak results

    // This can be fixed by deleting the pointer before reassigning it:
//    int value{ 5 };
//    int* ptr{ new int{} }; // allocate memory
//    delete ptr; // return memory back to operating system
//    ptr = &value; // reassign pointer to address of value

//    int* ptr{ new int{} };
//    ptr = new int{}; // old address lost, memory leak results

    return 0;
}
