// C++17 standard
// created by cicek on Jul 12, 2021 11:10 PM

// However, this constructor can still be used from inside the class
// (private access only prevents non-members from calling this function).
//A better way to resolve the issue is to use the “delete” keyword
// (introduced in C++11) to delete the function:

#include <iostream>
#include <string>

class MyString
{
private:
    std::string m_string;

public:
    MyString(char) = delete; // any use of this constructor is an error

    // explicit keyword makes this constructor ineligible for implicit conversions
    explicit MyString(int x) // allocate string of size x /
    {
        m_string.resize(x);
    }

    explicit MyString(
        const char *string) // allocate string to hold string value
    {
        m_string = string;
    }

    friend std::ostream &operator<<(std::ostream &out, const MyString &s);
};

std::ostream &operator<<(std::ostream &out, const MyString &s)
{
    out << s.m_string << '\n';
    return out;
}

int main()
{
    //    MyString mine('x'); // compile error, since MyString(char) is deleted
    //    std::cout << mine;

    MyString newString(10);
    std::cout << newString;


    return 0;
}
