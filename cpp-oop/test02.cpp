// C++17 standard
// created by cicek on Nov 03, 2021 2:09 PM

#include <iostream>

void print(int x)
{
    std::cout << x;
}

int main()
{
    std::cout << "Hello, World!" << '\n';
    print(static_cast<int>(5.3));
    return 0;
}
