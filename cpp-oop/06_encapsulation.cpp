// C++17 standard
// created by cicek on Jul 03, 2021 7:58 AM

#include <iostream>

int main()
{
    // Encapsulation (also called information hiding)
    // private (hiding the implementation details)

    // public:
    //    int getLength() { return m_length; } // access function to get value of m_length

    // Getters should provide “read-only” access to data.
    // Therefore, the best practice is that they should return by value or const reference (not by non-const reference).
    /*
     * A getter that returns a non-const reference would allow the caller to modify the
     * actual object being referenced, which violates the read-only nature of
     * the getter (and violates encapsulation).
     */

    // Getters should return by value or const reference.


    return 0;
}
