// C++17 standard
// created by cicek on Jul 14, 2021 12:00 PM

#include <iostream>

int main()
{
    std::cout << "Hello, World!" << '\n';
    return 0;
}
