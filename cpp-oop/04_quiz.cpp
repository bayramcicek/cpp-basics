// C++17 standard
// created by cicek on Jul 01, 2021 8:36 AM

#include <iostream>

class Point3d
{
private:
    int m_x;
    int m_y;
    int m_z;

public:
    void setValue(int a, int b, int c)
    {
        m_x = a;
        m_y = b;
        m_z = c;
    }

    void print() const { std::cout << m_x << m_y << m_z << '\n'; }

    bool isEqual(const Point3d &p) { return (p.m_x == this->m_x); }
};

int main()
{
    Point3d point{};
    point.setValue(13, 12, 38);

    Point3d newPoint{};
    newPoint.setValue(13,34,67);

    bool bIs = point.isEqual(newPoint);

    std::cout << bIs;

    return 0;
}
