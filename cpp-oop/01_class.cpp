// C++17 standard
// created by cicek on Jun 29, 2021 11:42 AM

#include <iostream>

class DateClass
{
public:
    int m_year{};
    int m_month{};
    int m_day{};

    // defines a member function named print()
    void print() const
    {
        std::cout << m_year << '/' << m_month << '/' << m_day;
    }
};

int main()
{
    DateClass today{ 2021, 06, 29 };
    today.print();

    /*
     * std::string s { "Hello, world!" }; // instantiate a string class object
    std::array<int, 3> a { 1, 2, 3 }; // instantiate an array class object
    std::vector<double> v { 1.1, 2.2, 3.3 }; // instantiate a vector class object

    std::cout << "length: " << s.length() << '\n'; // call a member function
     */

    return 0;
}
