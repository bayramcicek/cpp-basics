// C++17 standard
// created by cicek on Jul 05, 2021 3:43 PM

#include "Date.h"
#include <iostream>

// Date constructor
Date::Date(int year, int month, int day) { SetDate(year, month, day); }

// Date member function
void Date::SetDate(int year, int month, int day)
{
    m_month = month;
    m_day = day;
    m_year = year;
}

int main()
{
    Date my_date(2021, 9, 13);
    std::cout << my_date.getMonth();
    return 0;
}
