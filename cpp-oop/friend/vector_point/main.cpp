// C++17 standard
// created by cicek on Jul 05, 2021 7:34 PM

#include "Point3d.h" // for creating Point3d object
#include "Vector3d.h" // for creating Vector3d object
#include <iostream>

int main()
{
    Point3d p(2.0, 3.0, 4.0);
    Vector3d v(3.0, 2.0, 1.0);

    p.print();
    p.moveByVector(v);
    p.print();

    // Point(2 , 3 , 4)
    // Point(5 , 5 , 5)

    return 0;
}
