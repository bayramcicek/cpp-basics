// C++17 standard
// created by cicek on Jul 05, 2021 7:34 PM

#ifndef CPP_OOP_POINT3D_H
#define CPP_OOP_POINT3D_H

class Vector3d;

class Point3d
{
private:
    double m_x{};
    double m_y{};
    double m_z{};

public:
    explicit Point3d(double x = 0.0, double y = 0.0, double z = 0.0);

    void print() const;
    void moveByVector(const Vector3d &v);
};

#endif //CPP_OOP_POINT3D_H
