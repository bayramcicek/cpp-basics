// C++17 standard
// created by cicek on Jan 22, 2022 5:33 PM
// https://www.learncpp.com/cpp-tutorial/chapter-18-comprehensive-quiz/
#include <iostream>
// 2a, b

class Point
{
private:
    int m_x{};
    int m_y{};
    int m_z{};

public:
    Point(int x, int y, int z)
        : m_x{ x }
        , m_y{ y }
        , m_z{ z }
    {
    }

    friend std::ostream &operator<<(std::ostream &out, const Point &p)
    {
        return out << "Point(" << p.m_x << ", " << p.m_y << ", " << p.m_z
                   << ')';
    }
};

class Shape
{
public:
    virtual std::ostream &print(std::ostream &out) const = 0;

    friend std::ostream &operator<<(std::ostream &out, const Shape &p)
    {
        return p.print(out);
    }

    virtual ~Shape() = default;
};

class Triangle : public Shape
{
private:
    Point m_p1;
    Point m_p2;
    Point m_p3;

public:
    Triangle(const Point &p1, const Point &p2, const Point &p3)
        : m_p1(p1)
        , m_p2(p2)
        , m_p3(p3)
    {
    }

    std::ostream &print(std::ostream &out) const override
    {
        return out << "Triangle(" << m_p1 << ", " << m_p2 << ", " << m_p3
                   << ')';
    }
};

class Circle : public Shape
{
private:
    Point m_center;
    int m_radius;

public:
    Circle(const Point &center, int radius)
        : m_center(center)
        , m_radius(radius)
    {
    }

    std::ostream &print(std::ostream &out) const override
    {
        return out << "Circle(" << m_center << ", radius " << m_radius << ')';
    }
};

int main()
{
    Circle c{ Point{ 1, 2, 3 }, 7 };
    std::cout << c << '\n';

    Triangle t{ Point{ 1, 2, 3 }, Point{ 4, 5, 6 }, Point{ 7, 8, 9 } };
    std::cout << t << '\n';

    /*
     * Circle(Point(1, 2, 3), radius 7)
Triangle(Point(1, 2, 3), Point(4, 5, 6), Point(7, 8, 9))
     */

    return 0;
}
