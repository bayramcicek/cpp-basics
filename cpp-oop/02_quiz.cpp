// C++17 standard
// created by cicek on Jun 29, 2021 12:48 PM

#include <iostream>

class IntPair
{
public:
    int m_first{};
    int m_second{};

    void set(int a, int b)
    {
        m_first = a;
        m_second = b;
    }

    void print() { std::cout << m_first << ' ' << m_second << '\n'; }
};

int main()
{
    IntPair i{ 2, 4 };
    IntPair a{};
    a.print(); // 0, 0
    i.print(); // 2, 4

    i.set(78, 76);
    i.print(); // 78, 76

    /*
     * Why should we use a class for IntPair instead of a struct?
     * This object contains both member data and member functions,
     * so we should use a class. We should not use structs for objects that have member functions.
     */

    return 0;
}
