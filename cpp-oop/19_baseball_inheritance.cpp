// C++17 standard
// created by cicek on Dec 12, 2021 11:21 AM

#include <iostream>

class Person
{
private:
    int m_age;
    std::string m_name;

public:
    Person(int age = 0, const std::string &name = "")
        : m_age(age)
        , m_name(name)
    {
    }

    const std::string &getName() const { return m_name; };
    int getAge() const { return m_age; }
};

class BaseballPlayer : public Person
{
private:
    double m_battingAverage{};
    int m_homeRuns{};

public:
    BaseballPlayer(const std::string &name = "", int age = 0,
                   double battingAverage = 0.0, int homeRuns = 0)
        : Person(age, name)
        , m_battingAverage(battingAverage)
        , m_homeRuns(homeRuns)
    {
    }

    double getBattingAverage() const { return m_battingAverage; }
    int getHomeRuns() const { return m_homeRuns; }
};

int main()
{
    BaseballPlayer pedro{ "Pedro Cerrano", 32, 0.342, 42 };

    std::cout << pedro.getName() << '\n';
    std::cout << pedro.getAge() << '\n';
    std::cout << pedro.getHomeRuns() << '\n';

    return 0;
}
