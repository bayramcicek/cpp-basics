// C++17 standard
// created by cicek on Jul 01, 2021 8:46 AM

#include <array>
#include <cassert>
#include <iostream>

class Stack
{
private:
    // We use a std::array to store the elements
    using container_type = std::array<int, 10>;
    // For convenience, add a type alias for the type of the indexes
    using size_type = container_type::size_type;

    std::array<int, 10> arr{};
    size_type sizeOfStack{};

public:
    void reset() { sizeOfStack = 0; }

    bool push(int a)
    {
        if (sizeOfStack == arr.size())
            return false;
        arr[sizeOfStack++] = a;
        return true;
    }

    int pop()
    {
        assert(sizeOfStack > 0 && "Can not pop empty stack");
        return arr[--sizeOfStack];
    }

    void print()
    {
        std::cout << "( ";
        for (size_type i{ 0 }; i < sizeOfStack; ++i)
            std::cout << arr[i] << ' ';
        std::cout << ")\n";
    }

    void printAll(int a)
    {
        std::cout << "( ";
        for (size_type i{ 0 }; i < a; ++i)
            std::cout << arr[i] << ' ';
        std::cout << ")\n";
    }
};

int main()
{
    Stack stack{};

    stack.print();

    stack.push(5);
    stack.push(3);
    stack.push(8);
    stack.print();

    int a = stack.pop();
    std::cout << a;
    stack.print();

    stack.printAll(8);
    stack.print();

    return 0;
}
