// C++17 standard
// created by cicek on Jul 12, 2021 10:30 PM

#include <iostream>
#include <string>

class MyString
{
private:
    std::string m_string;

public:
    explicit MyString(int x) // allocate string of size x
    {
        m_string.resize(x);
    }

    explicit MyString(
        const char *string) // allocate string to hold string value
    {
        m_string = string;
    }

    friend std::ostream &operator<<(std::ostream &out, const MyString &s);
};

std::ostream &operator<<(std::ostream &out, const MyString &s)
{
    out << s.m_string;
    return out;
}

void printString(const MyString &s) { std::cout << s; }

int main()
{
//    MyString str{'x'}; // Allowed: initialization parameters may still be implicitly converted to match
//    std::cout << str << '\n';
//
//    printString(
//        static_cast<MyString>('x')); // Will compile and use MyString(int)
    return 0;
}