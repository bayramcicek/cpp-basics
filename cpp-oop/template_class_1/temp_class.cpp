// C++17 standard
// created by cicek on Jan 29, 2022 7:21 PM

#include "Array.h"
#include <iostream>

int main()
{
    int arrLength = 12;
    Array<int> intArray(arrLength);
    Array<double> doubleArray{ arrLength };

    for (int count{ 0 }; count < intArray.getLength(); ++count)
    {
        intArray[count] = count;
        doubleArray[count] = count + 0.5;
    }

    for (int count{ arrLength - 1 }; count >= 0; --count)
        std::cout << intArray[count] << '\t' << doubleArray[count] << '\n';

    return 0;
}
