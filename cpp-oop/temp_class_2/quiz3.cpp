// C++17 standard
// created by cicek on Feb 21, 2022 12:54 PM

#include <iostream>

template <typename T>
class Pair1
{
private:
    T m_val1{};
    T m_val2{};

public:
    Pair1(const T &val1, const T &val2)
        : m_val1(val1)
        , m_val2(val2)
    {
    }

    const T &first() const { return m_val1; }
    const T &second() const { return m_val2; }
};

template <typename T, typename M>
class Pair
{
private:
    T m_tVal{};
    M m_mVal{};

public:
    Pair(const T &tVal, const M &mVal)
        : m_tVal(tVal)
        , m_mVal(mVal)
    {
    }

    const T &first() const { return m_tVal; }
    const M &second() const { return m_mVal; }
};

template <typename S>
class StringValuePair : public Pair<std::string, S>
{
public:
    StringValuePair(const std::string &key, const S &value)
        : Pair<std::string, S>(key, value)
    {
    }
};

int main()
{
    //    Pair1<int> p1{ 5, 8 };
    //    std::cout << "Pair: " << p1.first() << ' ' << p1.second() << '\n';
    //
    //    const Pair1<double> p2{ 2.3, 4.5 };
    //    std::cout << "Pair: " << p2.first() << ' ' << p2.second() << '\n';

    //    Pair<int, double> p1{ 5, 6.7 };
    //    std::cout << "Pair: " << p1.first() << ' ' << p1.second() << '\n';
    //
    //    const Pair<double, int> p2{ 2.3, 4 };
    //    std::cout << "Pair: " << p2.first() << ' ' << p2.second() << '\n';

    StringValuePair<int> svp{ "Hello", 5 };
    std::cout << "Pair: " << svp.first() << ' ' << svp.second() << '\n';

    return 0;
}
