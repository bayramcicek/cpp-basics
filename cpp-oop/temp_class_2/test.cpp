// C++17 standard
// created by cicek on Feb 19, 2022 6:12 PM

#include <iostream>

template <typename T> class Storage
{
private:
    T m_value{};

public:
    Storage(T value)
        : m_value(value)
    {
    }

    void print() { std::cout << m_value << '\n'; }
    void printDouble();
};

// template <typename T>
// void Storage<T>::printDouble() {}
template <>
void Storage<double>::printDouble()
{
    std::cout << std::scientific << m_value << '\n';
}
template <>
void Storage<double>::print()
{
    std::cout << std::scientific << m_value << '\n';
}

int main()
{
    Storage<int> nValue(5);
    Storage<double> dValue(6.7);

    nValue.print();
    dValue.print(); // 6.7
    dValue.printDouble(); // 6.700000e+00

    std::cout << "Hello world\n";
    return 0;
}
