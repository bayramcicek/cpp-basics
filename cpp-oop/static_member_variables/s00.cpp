// C++17 standard
// created by cicek on Jul 05, 2021 4:47 PM

#include <iostream>

// Member variables of a class can be made static by using the static keyword.
// Unlike normal member variables, static member variables are shared by all objects of the class.
class Something
{
public:
    static int s_value; // declares the static member variable
};
// defines the static member variable (we'll discuss this section below)
int Something::s_value{ 1 };

int main()
{
    // note: we're not instantiating any objects of type Something

    Something::s_value = 2;
    std::cout << Something::s_value << '\n'; // 2
    return 0;
}
