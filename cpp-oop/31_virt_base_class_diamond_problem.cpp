// C++17 standard
// created by cicek on Jan 20, 2022 10:38 AM

#include <iostream>

// diamond problem
class PoweredDevice
{
public:
    PoweredDevice(int power)
    {
        std::cout << "PoweredDevice: " << power << '\n';
    }
};

class Scanner : public PoweredDevice
{
public:
    Scanner(int scanner, int power)
        : PoweredDevice{ power }
    {
        std::cout << "Scanner: " << scanner << '\n';
    }
};

class Printer : public PoweredDevice
{
public:
    Printer(int printer, int power)
        : PoweredDevice{ power }
    {
        std::cout << "Printer: " << printer << '\n';
    }
};

class Copier : public Scanner, public Printer
{
public:
    Copier(int scanner, int printer, int power)
        : Scanner{ scanner, power }
        , Printer{ printer, power }
    {
    }
};

int main()
{
    Copier copier(1, 2, 3);
    /*
PoweredDevice: 3
Scanner: 1
PoweredDevice: 3
Printer: 2
     */

    return 0;
}
