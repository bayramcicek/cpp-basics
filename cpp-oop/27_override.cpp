// C++17 standard
// created by cicek on Dec 18, 2021 10:33 PM

#include <iostream>
// One interesting note about covariant return types: C++ can’t dynamically select types,
// so you’ll always get the type that matches the actual version of the function being called.
class Base
{
public:
    // This version of getThis() returns a pointer to a Base class
    virtual Base *getThis()
    {
        std::cout << "called Base::getThis()\n";
        return this;
    }
    void printType() { std::cout << "returned a Base\n"; }
};

class Derived : public Base
{
public:
    // Normally override functions have to return objects of the same type as the base function
    // However, because Derived is derived from Base, it's okay to return Derived* instead of Base*
    Derived *getThis() override
    {
        std::cout << "called Derived::getThis()\n";
        return this;
    }
    void printType() { std::cout << "returned a Derived\n"; }
};

int main()
{
    Derived d{};
    Base *b{ &d };
    d.getThis()
        ->printType(); // calls Derived::getThis(), returns a Derived*, calls Derived::printType
    b->getThis()
        ->printType(); // calls Derived::getThis(), returns a Base*, calls Base::printType

    return 0;
}