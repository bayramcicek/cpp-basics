// C++17 standard
// created by cicek on Jul 05, 2021 4:15 PM

#include <iostream>

class Something
{
public:
    int m_value;

    Something(): m_value{0} { }

    void resetValue() { m_value = 0; }
    void setValue(int value) { m_value = value; }

    int getValue() const; // note addition of const keyword here
};
// getValue() has been marked as a const member function
int Something::getValue() const // and here
{
    return m_value;
}

int main()
{
    /*
const Date date1; // initialize using default constructor
const Date date2(2020, 10, 16); // initialize using parameterized constructor
const Date date3 { 2020, 10, 16 }; // initialize using parameterized constructor (C++11)

    const Something something{}; // calls default constructor

    something.m_value = 5; // compiler error: violates const
    something.setValue(5); // compiler error: violates const
 you’ll generally want to make your class objects const when you need to
     ensure they aren’t modified after creation.

     */

    // Make any member function that does not modify the state of the class object const, so that it can be called by const objects.

    std::cout << "Hello, World!" << '\n';
    return 0;
}
