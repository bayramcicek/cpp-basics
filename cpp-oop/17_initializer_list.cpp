// C++17 standard
// created by cicek on Nov 10, 2021 12:27 PM

#include <iostream>

int main()
{
    //    int array[]{ 1, 2, 3, 4, 6};

    int *array{ new int[5]{ 7, 8, 9, 0, 1 } };
    for (int i = 0; i < 5; i++)
    {
        std::cout << *(array + i) << ' ';
    }

    delete[] array;

    return 0;
}
