// C++17 standard
// created by cicek on Jul 12, 2021 9:57 AM

#include <iostream>

class Point
{
private:
    double m_x, m_y, m_z;

public:
    explicit Point(double x = 0.0, double y = 0.0, double z = 0.0)
        : m_x{ x }
        , m_y{ y }
        , m_z{ z }
    {
    }

    // Convert a Point into its negative equivalent
    Point operator-() const;

    // Return true if the point is set at the origin
    bool operator!() const;

    friend std::ostream &operator<<(std::ostream &out, const Point &point);

    [[nodiscard]] double getX() const { return m_x; }
    [[nodiscard]] double getY() const { return m_y; }
    [[nodiscard]] double getZ() const { return m_z; }
};

// Convert a Point into its negative equivalent
Point Point::operator-() const { return Point(-m_x, -m_y, -m_z); }

// Return true if the point is set at the origin, false otherwise
bool Point::operator!() const
{
    return (m_x == 0.0 && m_y == 0.0 && m_z == 0.0);
}

std::ostream &operator<<(std::ostream &out, const Point &point)
{
    out << "Point: " << point.m_x << '\t' << point.m_y << '\t' << point.m_z;
    return out;
}

int main()
{
    Point point{ 4.5, 2.2,
                 6.9 }; // use default constructor to set to (0.0, 0.0, 0.0)

    if (!point)
        std::cout << "point is set at the origin.\n";
    else
        std::cout << "point is not set at the origin.\n";

    std::cout << -point;

    return 0;
}
