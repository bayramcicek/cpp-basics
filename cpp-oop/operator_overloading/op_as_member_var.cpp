// C++17 standard
// created by cicek on Jul 11, 2021 6:55 PM

#include <iostream>

class Cents
{
private:
    int m_cents;

public:
    explicit Cents(int cents)
        : m_cents(cents)
    {
    }

    // Overload Cents + int
    Cents operator+(int value) const;

    [[nodiscard]] int getCents() const { return m_cents; }
};

// note: this function is a member function!
// the cents parameter in the friend version is now the implicit *this parameter
Cents Cents::operator+(int value) const { return Cents(m_cents + value); }

// cents1 + 2 becomes function call cents1.operator+(2).
// cents1.operator+(2) becomes operator+(&cents1, 2), which is almost identical to the friend version.

int main()
{
    Cents cents1(6);
    Cents cents2 = cents1 + 2;
    std::cout << "I have " << cents2.getCents() << " cents.\n";

    return 0;
}
