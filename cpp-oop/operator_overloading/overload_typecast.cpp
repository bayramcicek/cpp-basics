// C++17 standard
// created by cicek on Jul 12, 2021 9:08 PM

#include <iostream>

class Cents
{
private:
    int m_cents;

public:
    explicit Cents(int cents = 0)
        : m_cents{ cents }
    {
    }

    // Overloaded int cast
    explicit operator int() const { return m_cents; }

    [[nodiscard]] int getCents() const { return m_cents; }
    void setCents(int cents) { m_cents = cents; }
};

class Dollars
{
private:
    int m_dollars;

public:
    explicit Dollars(int dollars = 0)
        : m_dollars{ dollars }
    {
    }

    // Allow us to convert Dollars into Cents
    explicit operator Cents() const { return Cents(m_dollars * 100); }
};

void printCents(Cents cents)
{
    std::cout << static_cast<int>(cents); // cents will be implicitly cast to an int here
}

int main()
{
    Dollars dollars{ 9 };
    printCents(static_cast<Cents>(dollars)); // dollars will be implicitly cast to a Cents here

    std::cout << '\n';

    return 0;
}
