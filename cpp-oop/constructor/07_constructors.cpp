// C++17 standard
// created by cicek on Jul 03, 2021 8:18 AM

#include <iostream>

int main()
{
    /*
A constructor is a special kind of class member function that is automatically
     called when an object of that class is instantiated. Constructors are
     typically used to initialize member variables of the class to appropriate
     default or user-provided values, or to do any setup steps necessary for the
     class to be used (e.g. open a file or database).
     */

    // Constructors must have the same name as the class (with the same capitalization)
    // Constructors have no return type (not even void)

    // the default constructor will be called immediately after memory is allocated for the object, and our object will be initialized.

    // Fraction fiveThirds{ 5, 3 }; // List initialization, calls Fraction(int, int)
    // Fraction threeQuarters(3, 4); // Direct initialization, also calls Fraction(int, int)


    return 0;
}
