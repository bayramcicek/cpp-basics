// C++17 standard
// created by cicek on Jul 03, 2021 10:25 AM

#include <iostream>

class Something
{
private:
    int m_value1;
    double m_value2;
    char m_value3;

public:
    Something()
        : m_value1(1)
        , m_value2{ 2.2 }
        , m_value3{ 'c' } // Initialize our member variables
    {
        // No need for assignment here
    }

    Something(int value1, double value2, char value3 = 'c')
        : m_value1{ value1 }
        , m_value2{ value2 }
        , m_value3{ value3 } // directly initialize our member variables
    {
        // No need for assignment here
    }

    void print() const
    {
        std::cout << "Something(" << m_value1 << ", " << m_value2 << ", "
                  << m_value3 << ")\n";
    }
};

int main()
{
    // Assigning values to const or reference member variables in the body of the constructor is clearly not possible in some cases.
    // use:
    //Member initializer lists

    //you learned that you could initialize variables in three ways: copy, direct, and via uniform initialization:
    //int value1 = 1; // copy initialization
    //double value2(2.2); // direct initialization
    //char value3 {'c'}; // uniform initialization

    Something something{};
    something.print();

    return 0;
}
