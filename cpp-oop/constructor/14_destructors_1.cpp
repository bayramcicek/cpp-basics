// C++17 standard
// created by cicek on Jul 05, 2021 2:14 PM

#include <iostream>

class Simple
{
private:
    int m_nID{};

public:
    explicit Simple(int nID)
        : m_nID{ nID }
    {
        std::cout << "Constructing Simple " << nID << '\n';
    }

    ~Simple() { std::cout << "Destructing Simple" << m_nID << '\n'; }

    int getID() const { return m_nID; }
};

int main()
{
    // Allocate a Simple on the stack
    Simple simple{ 1 };
    std::cout << simple.getID() << '\n';

    // Allocate a Simple dynamically (heap)
    Simple *pSimple{ new Simple{ 2 } };

    std::cout << pSimple->getID() << '\n';
    std::cout << (*pSimple).getID() << '\n';

    // We allocated pSimple dynamically, so we have to delete it.
    delete pSimple;

    return 0;
} // simple goes out of scope here
