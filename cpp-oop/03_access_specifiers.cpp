// C++17 standard
// created by cicek on Jul 01, 2021 8:20 AM

#include <iostream>

class DateClass // members are private by default
{
    int m_month; // private by default, can only be accessed by other members
    int m_day; // private by default, can only be accessed by other members
    int m_year; // private by default, can only be accessed by other members
};

int main()
{
    DateClass date{};
    //    date.m_month = 10; // error

    // Make member variables private, and member functions public,
    // unless you have a good reason not to.

    std::cout << "Hello, World!" << '\n';
    return 0;
}
