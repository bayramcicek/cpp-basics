// C++17 standard
// created by cicek on Jul 05, 2021 3:04 PM

#include <iostream>

// The hidden *this pointer

int main()
{
    /*
     * Simple simple{1};
    simple.setID(2);

        simple.setID(2);
        the same as
        setID(&simple, 2); // note that simple has been changed from an object prefix to a function argument!

     void setID(int id) { m_id = id; }
     the same as
     void setID(Simple* const this, int id) { this->m_id = id; }

      The this pointer is a hidden const pointer that holds the address of the object the member function was called on.

     */
    return 0;
}
