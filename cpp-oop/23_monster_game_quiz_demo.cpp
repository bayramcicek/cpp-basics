// C++17 standard
// created by cicek on Dec 14, 2021 4:27 PM

#include <array>
#include <cstdlib> // for rand() and srand()
#include <ctime> // for time()
#include <iostream>
#include <string_view> // std::string_view requires C++17

/* https://www.learncpp.com/cpp-tutorial/chapter-17-comprehensive-quiz/  ------- Question #3-------
 * The following quiz question is more difficult and lengthy.
 * We’re going to write a simple game where you fight monsters.
 * The goal of the game is to collect as much gold as you can before you die or get to level 20.

Our program is going to consist of 3 classes:
 A Creature class, a Player class, and a Monster class. Player and Monster both inherit from Creature.*/
// ***********************

// Generate a random number between min and max (inclusive)
// Assumes std::srand() has already been called
// Assumes max - min <= RAND_MAX
int getRandomNumber(int min, int max)
{
    static constexpr double fraction{
        1.0 / (RAND_MAX + 1.0)
    }; // static used for efficiency, so we only calculate this value once
    // evenly distribute the random number across our range
    return min + static_cast<int>((max - min + 1) * (std::rand() * fraction));
}

class Creature
{
protected:
    std::string m_name;
    char m_symbol;
    int m_health;
    int m_damage; // damage per attack
    int m_gold;

public:
    Creature(std::string_view name, char symbol, int health, int damage,
             int gold)
        : m_name(name)
        , m_symbol(symbol)
        , m_health(health)
        , m_damage(damage)
        , m_gold(gold)
    {
    }

    void setName(const std::string &mName) { m_name = mName; }
    void setSymbol(char mSymbol) { m_symbol = mSymbol; }
    void setHealth(int mHealth) { m_health = mHealth; }
    void setDamage(int mDamage) { m_damage = mDamage; }
    void setGold(int mGold) { m_gold = mGold; }

    const std::string &getName() const { return m_name; }
    char getSymbol() const { return m_symbol; }
    int getHealth() const { return m_health; }
    int getDamage() const { return m_damage; }
    int getGold() const { return m_gold; }

    void reduceHealth(int reduceAmount) { m_health -= reduceAmount; }
    bool isDead() const { return m_health <= 0; }
    void addGold(int goldAmount) { m_gold += goldAmount; }
};

class Player : public Creature
{
private:
    int playerLevel = 1;

public:
    explicit Player(std::string_view name)
        : Creature(name, '@', 10, 1, 0)
    {
    }

    void levelUp()
    {
        ++playerLevel;
        ++m_damage;
    }

    int getLevel() const { return playerLevel; }
    bool hasWon() const { return playerLevel == 20; }
};

class Monster : public Creature
{
public:
    enum class Type
    {
        dragon,
        orc,
        slime,
        max_types
    };

private:
    static const Creature &getDefaultCreature(Type type)
    {
        static const std::array<Creature,
                                static_cast<std::size_t>(Type::max_types)>
            monsterData{ { { "dragon", 'D', 20, 4, 100 },
                           { "orc", 'o', 4, 2, 25 },
                           { "slime", 's', 1, 1, 10 } } };

        return monsterData.at(static_cast<std::size_t>(type));
    }

public:
    Monster(Type type)
        : Creature{ getDefaultCreature(type) }
    {
    }

    static Monster getRandomMonster()
    {
        int num{ getRandomNumber(0, static_cast<int>(Type::max_types) - 1) };
        return Monster{ static_cast<Type>(num) };
    }
};

int main()
{
    // ************************************
    //    Creature o{ "orc", 'o', 4, 2, 10 };
    //    o.addGold(5);
    //    o.reduceHealth(1);
    //    std::cout << "The " << o.getName() << " has " << o.getHealth()
    //              << " health and is carrying " << o.getGold() << " gold.\n";
    //
    //    std::string myName;
    //    std::cout << "Enter your name: ";
    //    std::cin >> myName;
    //
    //    Player play(myName);
    //
    //    std::cout << "You have " << play.getHealth() << " health and are carrying "
    //              << play.getGold() << " gold\n";
    // ************************************
    //    Monster m{ Monster::Type::orc };
    //    std::cout << "A " << m.getName() << " (" << m.getSymbol()
    //              << ") was created.\n"; // A orc (o) was created.
    // ************************************

    //    std::srand(static_cast<unsigned int>(
    //        std::time(nullptr))); // set initial seed value to system clock
    //    std::rand(); // get rid of first result
    //
    //    for (int i{ 0 }; i < 10; ++i)
    //    {
    //        Monster m{ Monster::getRandomMonster() };
    //        std::cout << "A " << m.getName() << " (" << m.getSymbol()
    //                  << ") was created.\n";
    //    }
    //    /*
    //     * A dragon (D) was created.
    //A orc (o) was created.
    //A dragon (D) was created.
    //A orc (o) was created.
    //A dragon (D) was created.
    //A slime (s) was created.
    //A orc (o) was created.
    //A orc (o) was created.
    //A orc (o) was created.
    //A slime (s) was created.*/

    // ************************************

    return 0;
}
