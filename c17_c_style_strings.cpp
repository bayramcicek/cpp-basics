// C++17 standard
// created by cicek on Jun 22, 2021 7:08 AM

#include <iostream>
#include <cstring>

int main()
{
    // A C-style string is called a null-terminated string.
    char myString[]{ "string" };

//    char name[255]; // declare array large enough to hold 255 characters
//    std::cout << "Enter your name: ";
//    std::cin.getline(name, std::size(name));
//    std::cout << "You entered: " << name << '\n';

    char source[]{"copy this"};
    char dest[50];
    std::strcpy(dest, source);
    std::cout << dest << '\n';

    std::cout << std::strlen(dest) << '\n';  // 9
    std::cout << std::size(dest) << '\n'; // 50

    return 0;
}
