// C++17 standard
// created by cicek on May 20, 2021 11:25 PM

#ifndef CPP_PROJECTS_GROWTH_H
#define CPP_PROJECTS_GROWTH_H

namespace basicMath
{
// the constant e is also part of namespace basicMath
inline constexpr double e{ 2.7 };
}

#endif //CPP_PROJECTS_GROWTH_H
