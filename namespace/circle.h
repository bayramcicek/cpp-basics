// C++17 standard
// created by cicek on May 20, 2021 11:23 PM

#ifndef CPP_PROJECTS_CIRCLE_H
#define CPP_PROJECTS_CIRCLE_H

namespace basicMath
{
inline constexpr double pi{ 3.14 };
}

#endif //CPP_PROJECTS_CIRCLE_H
