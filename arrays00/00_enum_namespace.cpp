// C++17 standard
// created by cicek on Jun 08, 2021 8:58 AM

#include <iostream>

namespace YeniNaneSpace
{
enum OgrenciEnum
{
    kenny,
    kyle,
    last
};
}

enum StudentNames
{
    kenny, // 0
    kyle, // 1
    stan, // 2
    butters, // 3
    cartman, // 4
    max_students // 5
};

enum class Hey
{
    test1,
    test2,
    last
};

int main()
{
    int a[5]{ 1, 2, 3, 4 };
    std::cout << a[4] << '\n'; // 0 -> default

    // Initialize all elements to 0.0
    double array[5]{};

    // Initialize all elements to an empty string
    std::string arrayw[5];

    // uninitialized
    double arrayq[5];

    int abc[]{ 1, 23, 45 };

    std::cout << abc[65];

    int testScores[max_students]{}; // allocate 5 integers
    testScores[stan] = 76;

    int testler[static_cast<int>(Hey::last)]{};
    testler[static_cast<int>(Hey::test2)] = 65;

    // always static_cast<int>() ??
    // it might be better to use a standard enum inside of a namespace

    int yeniList[YeniNaneSpace::last]{};
    yeniList[YeniNaneSpace::kyle] = 34;

    std::cout << yeniList[YeniNaneSpace::kyle];

    return 0;
}
