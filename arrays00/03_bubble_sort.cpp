// C++17 standard
// created by cicek on Jun 21, 2021 12:53 PM

#include <iostream>
#include <utility>

int main()
{
    int sortArray[]{ 6, 3, 2, 9, 7, 1, 5, 4, 8 };
    constexpr int my_length = static_cast<int>(std::size(sortArray));

    // bubble sort
    for (int limit = my_length - 2; limit > 0; --limit)
    {
        for (int i = 0; i <= limit; ++i)
        {
            if (sortArray[i + 1] < sortArray[i])
                std::swap(sortArray[i + 1], sortArray[i]);
        }
    }

    for (int j : sortArray)
    {
        std::cout << j << ' ';
    }

    return 0;
}
