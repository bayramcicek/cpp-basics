// C++17 standard
// created by cicek on May 15, 2021 7:36 PM

#include <iostream>

int main()
{
    std::cout << std::boolalpha; // print bools as true or false

    std::cout << true << '\n';
    std::cout << false << '\n';

    // You can use std::noboolalpha to turn it back off.
    std::cout << std::noboolalpha;

    return 0;
}
