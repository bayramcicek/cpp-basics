// C++17 standard
// created by cicek on May 15, 2021 8:12 PM

#include <iostream>
#include <string>

int main()
{
    std::string myStr{ "Alex" };
    myStr = "John";

    std::string myID{ "34" };

    std::cout << myStr << "\n";

    //    std::cout << "Enter name: ";
    //    std::string name{};
    //    std::getline(std::cin >> std::ws, name);
    //    std::cout << name << '\n';
    //    //  The std::ws input manipulator tells std::cin to ignore any
    //    //  leading whitespace. Note that std::ws is not a function.

    std::cout << myStr.length() << '\n'; // 4

    int bin{};
    bin = 0x01; // assign binary 0000 0001 to the variable
    bin = 0x02; // assign binary 0000 0010 to the variable
    bin = 0x04; // assign binary 0000 0100 to the variable

    std::cout << bin;

    return 0;
}
