// C++17 standard
// created by cicek on May 11, 2021 9:06 PM

#include <cstddef> // std::size_t
#include <iostream>

int main()
{
    /*
     * We can infer that operator sizeof returns an integer value -- but what
     * integer type is that value? An int? A short? The answer is that sizeof
     * (and many functions that return a size or length value) return a value
     * of type std::size_t. std::size_t is defined as an unsigned integral type,
     * and it is typically used to represent the size or length of objects.

Amusingly, we can use the sizeof operator (which returns a value of type
     std::size_t) to ask for the size of std::size_t itself:
     */
    std::cout << sizeof(int) << '\n'; // 4
    std::cout << sizeof(std::size_t) << '\n'; // 8
    return 0;
}
