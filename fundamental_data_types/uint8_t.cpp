// C++17 standard
// created by cicek on May 11, 2021 6:46 PM

#include <iostream>
#include <cstdint>

int main()
{
//    //  C99 defined a set of fixed-width integers (in the stdint.h header)
//    std::int32_t i{57};
//    std::cout << i;

    /*
     * The fixed-width integers have two downsides: First, they are optional
     * and only exist if there are fundamental types matching their widths and
     * following a certain binary representation. Using a fixed-width integer
     * makes your code less portable, it might not compile on other systems.
Second, if you use a fixed-width integer, it may also be slower than a wider
     type on some architectures. If you need an integer to hold values from
     -10 to 20, you might be tempted to use std::int8_t. But your CPU might be
     better at processing 32-bit wide integers, so you just lost speed by
     making a restriction that wasn’t necessary.
     */

    // Warning
    //
    //The above fixed-width integers should be avoided,
    // as they may not be defined on all target architectures.

    std::cout << "fast 8: " << sizeof(std::int_fast8_t) * 8 << " bits\n";
    std::cout << "fast 16: " << sizeof(std::int_fast16_t) * 8 << " bits\n";
    std::cout << "fast 32: " << sizeof(std::int_fast32_t) * 8 << " bits\n";

    std::cout << "least 8: " << sizeof(std::int_least8_t) * 8 << " bits\n";
    std::cout << "least 16: " << sizeof(std::int_least16_t) * 8 << " bits\n";
    std::cout << "least 32: " << sizeof(std::int_least32_t) * 8 << " bits\n";

    /*
fast 8: 8 bits
fast 16: 64 bits
fast 32: 64 bits

least 8: 8 bits
least 16: 16 bits
least 32: 32 bits
     */

    std::int8_t myint{65};
    std::cout << myint; // A
    // Avoid the 8-bit fixed-width integer types. If you do use them, note
    // that they are often treated like chars.

    /*
     * Avoid the following if possible:

Unsigned types, unless you have a compelling reason.
The 8-bit fixed-width integer types.
Any compiler-specific fixed-width integers -- for example,
     Visual Studio defines __int8, __int16, etc…
     */

    return 0;
}
