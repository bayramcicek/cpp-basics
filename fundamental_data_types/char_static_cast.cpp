// C++17 standard
// created by cicek on May 15, 2021 7:58 PM

#include <iostream>

int main()
{
    char ch1{ 'a' };
    std::cout << ch1 << '\n';

    /*
     * A type cast creates a value of one type from a value of another type.
     * To convert between fundamental data types (for example, from
     * a char to an int, or vice versa), we use a type cast called a static cast.

    static_cast<new_type>(expression)

     Whenever you see C++ syntax (excluding the preprocessor) that makes use
     of angled brackets(<>), the thing between the angled brackets will most
     likely be a type. This is typically how C++ deals with concepts that
     need a parameterizable type.

     */

    std::cout << static_cast<int>(ch1) << '\n'; // 97
    // The variable is not affected by casting its value to a new type.
    // In the above case, variable ch is still a char, and still holds the same value.
    std::cout << ch1 << '\n'; // a

    std::cout << "6D in hex is char '\x6D'\n"; // m

    return 0;
}
