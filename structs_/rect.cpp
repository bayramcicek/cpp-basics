// C++17 standard
// created by cicek on May 30, 2021 1:44 PM

#include <iostream>

struct Rectangle
{ // Non-static member initialization
    double length{ 1.0 };
    double width{ 1.0 };
};

int main()
{
    Rectangle x{}; // length = 1.0, width = 1.0
    x.length = 2.0; // you can assign other values like normal

    std::cout << "Hello world\n";
    return 0;
}
