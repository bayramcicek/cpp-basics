// C++17 standard
// created by cicek on May 30, 2021 1:38 PM

#include <iostream>

struct Employee
{ // no memory is allocated at this time
    int id;
    int age;
    double wage;
};

int main()
{
    Employee bayram{}; // create an Employee struct for Joe
    bayram.id = 1;
    bayram.age = 23;
    bayram.wage = 1000;

    Employee joe{ 25, 22, 23 };
    /*
    joe = { 1, 32, 60000.0 };
// This is the same as
    joe = Employee{ 1, 32, 60000.0 };
*/
    std::cout << joe.wage << '\n';
    std::cout << bayram.wage << '$';
    return 0;
}
