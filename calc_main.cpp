// C++17 standard
// created by cicek on Dec 04, 2021 10:34 AM

#include "calc.h"
#include <iostream>

int main()
{
    int first = 0;
    int second = 0;
    int operation = 0;

    std::cout << "enter first number: ";
    std::cin >> first;

    std::cout << "enter second number: ";
    std::cin >> second;

    std::cout << "add: 1\n"
              << "sub: 2\n"
              << "mul: 3\n"
              << "div: 4\n"
        << "enter: ";

    std::cin >> operation;

    std::cout << "res: " << calculate(operation, first, second);

    return 0;
}
