// C++17 standard
// created by cicek on Jun 24, 2021 9:32 PM

#include <algorithm>
#include <array>
#include <iostream>

int main()
{
    /*
     * The functionality provided in the algorithms library generally fall into one of three categories:

Inspectors -- Used to view (but not modify) data in a container. Examples include searching and counting.
Mutators -- Used to modify data in a container. Examples include sorting and shuffling.
Facilitators -- Used to generate a result based on values of the data members.
     Examples include objects that multiply values, or objects that determine what order pairs of elements should be sorted in.
     *
     * */
    std::array arr{ 13, 90, 99, 5, 40, 80 };

    std::cout << "Enter a value to search for and replace with: ";
    int search{};
    int replace{};
    std::cin >> search >> replace;

    // Input validation omitted

    // std::find returns an iterator pointing to the found element (or the end of the container)
    // we'll store it in a variable, using type inference to deduce the type of
    // the iterator (since we don't care)
    auto found{ std::find(arr.begin(), arr.end(), search) };

    // Algorithms that don't find what they were looking for return the end iterator.
    // We can access it by using the end() member function.
    if (found == arr.end())
    {
        std::cout << "Could not find " << search << '\n';
    }
    else
    {
        // Override the found element.
        *found = replace;
    }

    for (int i : arr)
    {
        std::cout << i << ' ';
    }

    std::cout << '\n';

    return 0;
}
