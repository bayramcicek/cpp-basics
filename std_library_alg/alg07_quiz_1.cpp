// C++17 standard
// created by cicek on Jun 24, 2021 11:51 PM

/*
 * Pretend you’re writing a game where the player can hold 3 types of items:
 * health potions, torches, and arrows. Create an enum to identify the different
 * types of items, and an std::array to store the number of each item the player
 * is carrying (The enumerators are used as indexes of the array). The player should
 * start with 2 health potions, 5 torches, and 10 arrows. Write a function called
 * countTotalItems() that returns how many items the player has in total.
 * Have your main() function print the output of countTotalItems() as well as the number of torches.
 */

//#include <array>
//#include <iostream>
//
//enum Player
//{
//    potion,
//    torch,
//    arrow,
//    top_item
//};
//
//int countTotalItems(const std::array<int, top_item> arr)
//{
//    int total = 0;
//    for (auto i : arr)
//    {
//        total += i;
//    }
//
//    return total;
//}
//
//int main()
//{
//    std::array Item{ 2, 5, 10 };
//
//
//    std::cout << Item[torch] << '\n';
//    std::cout << countTotalItems(Item) << '\n';
//
//    return 0;
//}

#include <array>
#include <iostream>
#include <numeric> // std::reduce

// We want to use ItemTypes to index an array. Use enum rather than enum class.
enum ItemTypes
{
    item_health_potion,
    item_torch,
    item_arrow,
    max_items
};

using inventory_type = std::array<int, ItemTypes::max_items>;

int countTotalItems(const inventory_type &items)
{
    return std::reduce(items.begin(), items.end());
}

int main()
{
    inventory_type items{ 2, 5, 10 };

    std::cout << "The player has " << countTotalItems(items)
              << " item(s) in total.\n";

    // We can access individual items using the enumerators:
    std::cout << "The player has " << items[ItemTypes::item_torch]
              << " torch(es)\n";

    return 0;
}