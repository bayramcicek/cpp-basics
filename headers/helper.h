// C++17 standard
// created by cicek on May 06, 2021 8:58 PM

// If a header file is paired with a code file (e.g. add.h with add.cpp),
// they should both have the same base name (add).

/*
 * The good news is that we can avoid the above problem via a mechanism called
 * a header guard (also called an include guard). Header guards are conditional
 * compilation directives that take the following form:
 */
#ifndef CPP_PROJECTS_HELPER_H
#define CPP_PROJECTS_HELPER_H
/*
 * Header files allow us to put declarations in one location and then import
 * them wherever we need them. This can save a lot of typing in multi-file programs.
 */

void fun();

#endif //CPP_PROJECTS_HELPER_H
