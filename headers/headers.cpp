// C++17 standard
// created by cicek on May 06, 2021 8:51 PM

// When we use angled brackets, we’re telling the preprocessor that this is a
// header file we didn’t write ourselves.
#include <iostream>
/*
 * std::cout has been forward declared in the “iostream” header file.
 * When we #include <iostream>, we’re requesting that the preprocessor
 * copy all the content (including forward declarations for std::cout)
 * from the file named “iostream” into the file doing the #include.
 */
#include "helper.h"
// When we use double-quotes, we’re telling the preprocessor that this is a header file that we wrote.
//  The compiler will first search for the header file in the current directory.
//  If it can’t find a matching header there, it will then search the include directories.

int main()
{
    std::cout << "Hello world\n";
    fun();
    return 0;
}
/*
 * RULE:
 * Use double quotes to include header files that you’ve written or are
 * expected to be found in the current directory. Use angled brackets to include
 * headers that come with your compiler, OS, or third-party libraries you’ve
 * installed elsewhere on your system.
 */

/*
 * Best practice

When including a header file from the standard library, use the no extension
 version (without the .h) if it exists. User-defined headers should still
 use a .h extension.
 */