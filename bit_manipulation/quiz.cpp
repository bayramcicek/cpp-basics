// C++17 standard
// created by cicek on May 18, 2021 9:13 PM

#include <bitset>
#include <iostream>

std::bitset<4> rotateLeft(std::bitset<4> bits)
{
    // Your code here
    const bool leftmost{ bits.test(3) };
    bits <<= 1;

    if (leftmost)
    {
        bits.set(0);
    }

    return bits;

    // Redo quiz #2 but don't use the test and set functions.
    /*
// bits << 1 does the left shift
// bits >> 3 handle the rotation of the leftmost bit
return (bits<<1) | (bits>>3);
     */
}

int main()
{
    std::bitset<4> bits1{ 0b0001 };
    std::cout << rotateLeft(bits1) << '\n';

    std::bitset<4> bits2{ 0b1001 };
    std::cout << rotateLeft(bits2) << '\n';

    /*
and print the following:

0010
0011
     */

    return 0;
}