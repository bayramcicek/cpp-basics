// C++17 standard
// created by cicek on May 16, 2021 9:29 PM

#include <bitset> // for std::bitset
#include <iostream>

int main()
{
    /*Modifying individual bits within an object is called bit manipulation.*/
    std::bitset<8> mybitset{}; // 8 bits in size means room for 8 flags
    // Bit manipulation is one of the few times when you should unambiguously
    // use unsigned integers (or std::bitset).

    /*
     * Each number denotes a bit position.
     *
     *  76543210  Bit position
        00000101  Bit sequence
     */

    std::bitset<8> bits{
        0b0000'0101
    }; // we need 8 bits, start with bit pattern 0000 0101
    bits.set(3); // set bit position 3 to 1 (now we have 0000 1101)
    bits.flip(4); // flip bit 4 (now we have 0001 1101)
    bits.reset(4); // set bit 4 back to 0 (now we have 0000 1101)

    std::cout << "All the bits: " << bits << '\n';
    std::cout << "Bit 3 has value: " << bits.test(3) << '\n';
    std::cout << "Bit 4 has value: " << bits.test(4) << '\n';

    return 0;
}
