// C++17 standard
// created by cicek on May 18, 2021 9:58 PM

#include <bitset>
#include <cstdint>
#include <iostream>

/*
 * A bit mask is a predefined set of bits that is used to select which
 * specific bits will be modified by subsequent operations.
 */

int main()
{
    // Because C++14 supports binary literals, defining these bit masks is easy:
    constexpr std::uint_fast8_t mask0{ 0b0000'0001 }; // represents bit 0
    constexpr std::uint_fast8_t mask1{ 0b0000'0010 }; // represents bit 1
    constexpr std::uint_fast8_t mask2{ 0b0000'0100 }; // represents bit 2
    constexpr std::uint_fast8_t mask3{ 0b0000'1000 }; // represents bit 3
    constexpr std::uint_fast8_t mask4{ 0b0001'0000 }; // represents bit 4
    constexpr std::uint_fast8_t mask5{ 0b0010'0000 }; // represents bit 5
    constexpr std::uint_fast8_t mask6{ 0b0100'0000 }; // represents bit 6
    constexpr std::uint_fast8_t mask7{ 0b1000'0000 }; // represents bit 7

    //    // C++11
    //    constexpr std::uint_fast8_t mask0{ 1 << 0 }; // 0000 0001
    //    constexpr std::uint_fast8_t mask1{ 1 << 1 }; // 0000 0010
    //    constexpr std::uint_fast8_t mask2{ 1 << 2 }; // 0000 0100
    //    constexpr std::uint_fast8_t mask3{ 1 << 3 }; // 0000 1000
    //    constexpr std::uint_fast8_t mask4{ 1 << 4 }; // 0001 0000
    //    constexpr std::uint_fast8_t mask5{ 1 << 5 }; // 0010 0000
    //    constexpr std::uint_fast8_t mask6{ 1 << 6 }; // 0100 0000
    //    constexpr std::uint_fast8_t mask7{ 1 << 7 }; // 1000 0000

    // To determine if a bit is on or off, we use bitwise AND in conjunction with the bit mask for the appropriate bit:
    std::uint_fast8_t flags{
        0b0000'0101
    }; // // 8 bits in size means room for 8 flags

    std::cout << "bit 0 is "
              << ((flags & mask0) ? "on\n" : "off\n"); // bit 0 is on
    std::cout << "bit 1 is "
              << ((flags & mask1) ? "on\n" : "off\n"); // bit 1 is off

    // Setting a bit
    // To set (turn on) a bit, we use bitwise OR equals (operator |=) in conjunction with the bit mask for the appropriate bit:
    flags |= mask1; // turn on bit 1
    std::cout << "bit 1 is "
              << ((flags & mask1) ? "on\n" : "off\n"); // bit 1 is on

    // Bitwise OR
    // flags |= (mask4 | mask5); // turn bits 4 and 5 on at the same time

    // resetting a bit
    flags &= ~(mask4 | mask5); // turn bits 4 and 5 off at the same time

    // Flipping a bit: To toggle a bit state, we use Bitwise XOR
    flags ^= (mask4 | mask5); // flip bits 4 and 5 at the same time

    // Bit masks and std::bitset
    std::bitset<8> flags_new{
        0b0000'0101
    }; // 8 bits in size means room for 8 flags

    std::cout << "bit 1 is " << (flags_new.test(1) ? "on\n" : "off\n");
    std::cout << "bit 2 is " << (flags_new.test(2) ? "on\n" : "off\n");
    flags_new ^= (mask1 | mask2); // flip bits 1 and 2

    std::cout << "bit 1 is " << (flags_new.test(1) ? "on\n" : "off\n");
    std::cout << "bit 2 is " << (flags_new.test(2) ? "on\n" : "off\n");
    flags_new |= (mask1 | mask2); // turn bits 1 and 2 on

    std::cout << "bit 1 is " << (flags_new.test(1) ? "on\n" : "off\n");
    std::cout << "bit 2 is " << (flags_new.test(2) ? "on\n" : "off\n");
    flags_new &= ~(mask1 | mask2); // turn bits 1 and 2 off

    std::cout << "bit 1 is " << (flags_new.test(1) ? "on\n" : "off\n");
    std::cout << "bit 2 is " << (flags_new.test(2) ? "on\n" : "off\n");

    return 0;
}
