// C++17 standard
// created by cicek on May 29, 2021 11:28 AM

#include <cmath>
#include <iostream>

int main()
{
    double x{};
//tryAgain: // this is a statement label
    //    std::cout << "Enter a non-negative number: ";
    //    std::cin >> x;
    //
    //    if (x < 0.0)
    //        goto tryAgain; // this is the goto statement
    //
    //    std::cout << "The square root of " << x << " is " << std::sqrt(x) << '\n';

    char myChar{ 'a' };
    while (myChar <= 'z')
    {
        std::cout << myChar << ' ' << static_cast<int>(myChar) << '\n';
        ++myChar;
    }

    for (int xx{ 0 }, y{ 5 }; xx < 10; ++xx, --y)
        std::cout << xx << ' ' << y << '\n';

    return 0;
}
