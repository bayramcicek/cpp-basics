// C++17 standard
// created by cicek on May 24, 2021 1:46 AM

#include "constants.h" // include a copy of each constant in this file
#include <iostream>

/*
 * When this header gets #included into a .cpp file, each of these variables
 * defined in the header will be copied into that code file at the point of
 * inclusion. Because these variables live outside a function, they’re
 * treated as global variables within the file they are included into,
 * which is why you can use them anywhere in that file.
 */
// if constants.h gets included into 20 different code files, each of these variables is duplicated 20 times.
int main()
{
    std::cout << "Enter a radius: ";
    int radius{};
    std::cin >> radius;

    std::cout << "The circumference is: " << 2.0 * radius * constants::pi
              << '\n';

    return 0;
}